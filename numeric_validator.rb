module Validate
  class NumericValidator < EachRecordValidator

    CHECKS = { odd: :odd,even: :even,
               equal_to: :==,
               greater_than: :>, greater_than_or_equal_to: :>=,
               less_than: :<, less_than_or_equal_to: :<=,
               other_than: :!= }.freeze


    def validate_each(record,attribute,value)
      unless value.respond_to?(:to_i) && value.to_i.is_a?(Numeric)
        record.error.add(attributes,"Not a numeric value",options)
      end

      keys = options.keys & CHECKS.keys

      keys.each do |key|
        case key
          when :odd,:even
            unless value.to_i.send(CHECKS[key])
              record.error.add(attribute,key,options)
            end
          else
            check_value = options[key]
            unless value.to_i.send(CHECK[key],check_value)
              record.error.add(attribute,key,options)
            end

        end
      end

    end
  end

end
