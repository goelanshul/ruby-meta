module Validate
  class LengthValidator < EachRecordValidaor

    POSSIBLE_CHECKS = { is: :==, minimum: :>=, maximum: :<= }.freeze
    ERROR_MESSAGES = { is: :lengths_dont_match, minimum: :length_is_greater_than_expected,maximum: :length_is_less_than_expected}


    def initialize(options)
      if options.keys.empty
        raise StandardError,"Range is a must."
      end

      if options[:allow_blank] == false && options[:minimum].nil? && options[:is].nil?
        options[:minimum] = 1
      else
        options[:minimum] = 0
      end

      super

    end

    def validate_each(record,attribute,value)
      value_length = value.length
      keys = POSSIBLE_CHECKS.keys & options.keys

      if keys.empty?
        raise StandardError,"No Range specified which is a must. Please use in,maximum or maximum."
      end

      keys.each do |key|
        check_operator = POSSIBLE_CHECKS[key]
        check_value = options[key]
        if ! (value.is_a?(Integer) && value>=0)
          raise StandardError,"Value specified must be a integer greater than  or equal to 0"
          elif ! value_length.send(check_operator,check_value)
          record.error.add(attribute,ERROR_MESSAGES[key],options[:message]||ERROR_MESSAGES[key])
        end
      end
    end
  end

end
