require_relative 'validate'


module Record
  module Validate

    class AbscenceValidator < EachRecordValidator
      def validate_each(record, attr_name, value)
        if value.present?
          record.errors.add(attr_name, :present, options[:message]||"Value should be absent")
        end
      end
    end

  end
end