module Record

  def test

  end

  module Validate
    def valid?(context = nil)
      error.empty?
    end
  end

  class Validator

    attr_accessor :options

    def initialize(options = {})
      @options = options.freeze
    end

    def self.name
      @kind = name.split('::').last.underscore.chomp('_validator').to_sym unless anonymous?
      print @kind
    end

    def validate(record)
      raise StandardError, "Child classes must implement this"
    end

    def self.kind
      @kind ||= name.split("::").last.underscore.chomp("_validator").to_sym unless anonymous?
    end


  end

  class EachRecordValidator < Validator

    attr_accessor :attributes

    def initialize(options={})
      puts "Level 0 : "+options.to_s
      @attributes = Array(options.delete(:attributes))
      if @attributes.empty?
        raise ArgumentError, ":attributes cannot be blank"
      end
      super
    end


    def validate(record)
      attributes.each do |attribute|
        value = record.read_attribute_for_validation(attribute)
        if (options[:allow_nil] && value.nil?) || (options[:allow_blank] && value.blank?)
          validate_each(record, attribute, value)
        end
      end

      def validate_each(record, attribute, value)
        raise StandardError, "Subclasses must implement this method"
      end


    end
  end

  class BlockValidator < EachRecordValidator
    def initialize(options={}, &block)
      @block = block
    end

    private

    def validate_each(record, attribute, value)
      @block.call(record, attribute, value)
    end
  end

  class WithValidator < EachRecordValidator # :nodoc:
    def validate_each(record, attr, val)
      method_name = options[:with]

      if record.method(method_name).arity == 0
        record.send method_name
      else
        record.send method_name, attr
      end
    end
  end

  module Validate

    class Array

      def extract_options!
        last.is_a?(::Hash) ? pop : {}
      end

    end


    module ClassMethods



      DEFAULT_KEYS = [:if, :unless, :on, :allow_blank, :allow_nil , :strict]

      def validate(*args)
        options = args.last.is_a?(::Hash) ? args.pop : {}

        validations = options.dup

        puts "Level 1: (Shows all validations imposed) "+validations.to_s

        DEFAULT_KEYS.each {|key|
        validations.delete(key)

        }



        raise ArgumentError, "You need to supply at least one attribute" if args.empty?
        raise ArgumentError, "You need to supply at least one validation" if validations.empty?

        options[:attributes] = args

        validations.each do |key, value|
          next unless value
          key = "#{key.capitalize}Validator"
          puts "Level 1 : Resulting Validator name "+key

          begin
            #p = PresenceValidator.new(presence: true)
            #names = key.split("::".freeze)
            #puts Object.const_get(key) if names.empty?
            validator = key.include?("::".freeze) ? key.constantize : const_get(key)
                #key.include?("::".freeze) ? key.constantize : const_get(key)
            puts "Level 1 : Validator Name "+validator.to_s
          #rescue NameError
          # raise ArgumentError, "Unknown validator: '#{key}'"
          end

          puts "Level 1 : Calling validates with "+ validator.to_s+"    "+ options.to_s
          ## Merge value hash with options
          validates_with(validator, _parse_validates_options(options))
        end

      end


      def validates_with(*args, &block)

        options = args.last.is_a?(::Hash) ? args.pop : {}
        options[:class] = self

        validators = Hash.new { |h,k| h[k] = [] }

        puts "Level 2 : Args "+args.to_s

        args.each do |klass|
          puts "Level 2 : Validator "+klass.to_s
          validator = klass.new(options, &block)

          puts "Level 2 : Validator attributes "+validator.attributes.to_s


          if validator.respond_to?(:attributes) && !validator.attributes.nil?
            validator.attributes.each do |attribute|
              puts "Level 2 : Attribute in validate_with is "+attribute.to_s
              validators[attribute.to_sym] << validator
            end
          else
            validators[nil] << validator
          end

          validators.each do |key,value|
            puts "Level 2 : Key "+key.to_s
            puts "Level 2 : Value "+value.to_s

          end
          puts "Level 2 : Attributes and their validators "+validators.to_s

          validates(validator,validators, options)
        end
      end



      def validates(*args, &block)

        validator = args[0]
        validators = args[1]
        options = args.last.is_a?(::Hash) ? args.pop : {}


        puts "Level 3 : options "+ options.to_s
        puts "Level 3 : validator "+validator.to_s
        puts "Level 3 : validators "+validators.to_s


        if args.all? { |arg| arg.is_a?(Symbol) }
          options.each_key do |k|
            unless DEFAULT_KEYS.include?(k)
              raise ArgumentError.new("Unknown key: #{k.inspect}. Valid keys are: #{DEFAULT_KEYS.map(&:inspect).join(', ')}. Perhaps you meant to call `validates` instead of `validate`?")
            end
          end
        end

        if options.key?(:on)
          options = options.dup
          options[:if] = Array(options[:if])
          options[:if].unshift ->(o) {
            !(Array(options[:on]) & Array(o.validation_context)).empty?
          }
        end

        args << options
        #add_callback(:validate, *args, &block)
      end


      def add_callback(name, *filter_list, &block)
        type, filters, options = normalize_callback_params(filter_list, block)
        self_chain = get_callbacks name
        mapped = filters.map do |filter|
          Callback.build(self_chain, filter, type, options)
        end

        __update_callbacks(name) do |target, chain|
          options[:prepend] ? chain.prepend(*mapped) : chain.append(*mapped)
          target.set_callbacks name, chain
        end
      end


      def normalize_callback_params(filters, block) # :nodoc:
        type = CALLBACK_FILTER_TYPES.include?(filters.first) ? filters.shift : :before
        options = filters.extract_options!
        filters.unshift(block) if block
        [type, filters, options.dup]
      end








    def _parse_validates_options(options) # :nodoc:
        case options
          when TrueClass
            {}
          when Hash
            options
          when Range, Array
            { in: options }
          else
            { with: options }
        end
      end

    end


  end
end
