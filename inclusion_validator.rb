require_relative 'validate'

module Record

  module Validate
    class IncludeValidator < EachRecordValidator

      def validate_each(record,attribute,value)
        unless include?(record,value)
          record.errors.add(attribute, :inclusion, options.except(:in, :within).merge!(value: value))
        end
      end

      protected

      def include?(record, value)
        members = if delimiter.respond_to?(:call)
                    delimiter.call(record)
                  elsif delimiter.respond_to?(:to_sym)
                    record.send(delimiter)
                  else
                    delimiter
                  end

        members.send(inclusion_method(members), value)
      end

      def delimiter
        @delimiter ||= options[:in] || options[:within]
      end

    end
  end

end
