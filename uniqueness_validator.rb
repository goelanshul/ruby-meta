module Validate

  class UniquenessValidator

    @@value_map
    def initialize(options)
      if @@value_map.nil?
        @@value_map = Hash.new(Hash.new)
      end
    end

    def validate_each(record,attribute,value)
      record_class = record.class.name.split('::').last
      #value = map_enum_attribute(record_class, attribute, value)

      if @@value_map[record_class].empty?
        record_hash = Hash.new
        record_hash[attribute] = value
        @@value_map[record_class] = record_hash
      else
        record_hash = @@value_map[record_class]
        unless record_hash[attribute].nil?
          record.error.add(attribute,:taken,options[:message]||"Value already used for #{attribute}")
        end
      end

    end

    protected
    # The check for an existing value should be run from a class that
    # isn't abstract. This means working down from the current class
    # (self), to the first non-abstract class. Since classes don't know
    # their subclasses, we have to build the hierarchy between self and
    # the record's class.
    def find_finder_class_for(record) #:nodoc:
      class_hierarchy = [record.class]

      while class_hierarchy.first != @klass
        class_hierarchy.unshift(class_hierarchy.first.superclass)
      end

      class_hierarchy.detect { |klass| !klass.abstract_class? }
    end

    def build_relation(klass, attribute, value) # :nodoc:
      if reflection = klass._reflect_on_association(attribute)
        attribute = reflection.foreign_key
        value = value.attributes[reflection.klass.primary_key] unless value.nil?
      end

      if value.nil?
        return klass.unscoped.where!(attribute => value)
      end

      # the attribute may be an aliased attribute
      if klass.attribute_alias?(attribute)
        attribute = klass.attribute_alias(attribute)
      end

      attribute_name = attribute.to_s

      table = klass.arel_table
      column = klass.columns_hash[attribute_name]
      cast_type = klass.type_for_attribute(attribute_name)

      comparison = if !options[:case_sensitive]
                     # will use SQL LOWER function before comparison, unless it detects a case insensitive collation
                     klass.connection.case_insensitive_comparison(table, attribute, column, value)
                   else
                     klass.connection.case_sensitive_comparison(table, attribute, column, value)
                   end
      klass.unscoped.tap do |scope|
        parts = [comparison]
        binds = [Relation::QueryAttribute.new(attribute_name, value, cast_type)]
        scope.where_clause += Relation::WhereClause.new(parts, binds)
      end
    end

    def scope_relation(record, relation)
      Array(options[:scope]).each do |scope_item|
        if reflection = record.class._reflect_on_association(scope_item)
          scope_value = record.send(reflection.foreign_key)
          scope_item  = reflection.foreign_key
        else
          scope_value = record._read_attribute(scope_item)
        end
        relation = relation.where(scope_item => scope_value)
      end

      relation
    end

    def map_enum_attribute(klass, attribute, value)
      mapping = klass.defined_enums[attribute.to_s]
      value = mapping[value] if value && mapping
      value
    end







  end

end
