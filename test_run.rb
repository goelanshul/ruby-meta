require_relative 'record'

class TestRun < Record::Base
  attr_accessor :name,:age

  def initialize(args = {})
    args.each do |key,value|
      send("#{key}=",value)
    end
  end
  validate :name, presence: true
  #validate :age, abscence: true
end


test = TestRun.new({:name => "Anshul",:age => 25})
#puts "Valid Test "+test.valid?.to_s
#puts "InValid Test "+test.valid?.to_s

