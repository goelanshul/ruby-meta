module Record

  class Errors

    attr_accessor :message, :details, :attribute

    ##This method is used for adding an error if any validator fails
    def add(attribute, message = :invalid, details)
      @message = message.call if message.respond_to?(:call)
      @details = details
      @attribute = attribute
    end

  end

end
