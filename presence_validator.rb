require_relative 'validate'
module Record
  module Validate

    class PresenceValidator < EachRecordValidator

      def validate_each(record,attribute,value)
        puts record.to_s
        if value.nil?
          record.error.add(attribute,:present,options)
        end
      end

    end
  end

end
