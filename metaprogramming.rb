class MetaProgramming
  attr_accessor :name, :type

  def assign_value(values={})
    values.each do |k, v|
      self.send("#{k}=", v)
    end
  end

  def add_new_method(name, &logic)
    self.class.send(:define_method, "#{name}") {
        |*args|
      logic.call(*args)
    }

  end

  def self.add_new_method(name, &logic)
    puts "Adding new method called "+name
    #define_method(name) {|*args|
    #  logic.call(*args)

    #}
    send(:define_method, name) {|*args|
      logic.call(*args)
    }

  end

  def self.add_new_class_method(name, &logic)
    puts "Adding new method called "+name
    #define_method(name) {|*args|
    #  logic.call(*args)

    #}
    self.class.send(:define_method, name) {|*args|
      logic.call(*args)
    }

  end


end


dummy = {:name => "Anshul Goel",
         :type => "Coder"}

p = MetaProgramming.new

p.assign_value dummy
puts p.inspect

MetaProgramming.add_new_method("add") do |x, y|
  puts "Sum is "+ (x+y).to_s
end

MetaProgramming.add_new_class_method("add") do |x, y|
  puts "Sum is "+ (x+y).to_s
end

#MetaProgramming.add(2, 3)

p.add_new_method("add_two") do |x,y|
  puts "Sum is "+ (x+y).to_s
end

p.add(5, 9)
p.add_two(1,4)
MetaProgramming.add(1,9)